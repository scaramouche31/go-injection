package libinjection

import (
	"fmt"
	"testing"
)

func TestXSSWorkflow(t *testing.T) {
	_, f := IsXSS(`onclick="alert(1)"`)
	fmt.Println(f)
}

func TestSQLiWorkflow(t *testing.T) {
	_, f := IsSQLi(`12345=12345||12345`)
	fmt.Println(f)
}
