package libinjection

import (
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

func TestIsXSS(t *testing.T) {
	testCases := []struct {
		payload   string
		malicious bool
	}{
		{"<script>alert(1);</script>", true},
		{"><script>alert(1);</script>", true},
		{"x ><script>alert(1);</script>", true},
		{"' ><script>alert(1);</script>", true},
		{"\"><script>alert(1);</script>", true},
		{"red;</style><script>alert(1);</script>", true},
		{"red;}</style><script>alert(1);</script>", true},
		{"red;\"/><script>alert(1);</script>", true},
		{"');}</style><script>alert(1);</script>", true},
		{"onerror=alert(1)>", true},
		{"x onerror=alert(1);>", true},
		{"x' onerror=alert(1);>", true},
		{"x\" onerror=alert(1);>", true},
		{"<a href=\"javascript:alert(1)\">", true},
		{"<a href='javascript:alert(1)'>", true},
		{"<a href=javascript:alert(1)>", true},
		{"<a href  =   javascript:alert(1); >", true},
		{"<a href=\"  javascript:alert(1);\" >", true},
		{"<a href=\"JAVASCRIPT:alert(1);\" >", true},

		// https://github.com/client9/libinjection/issues/160
		{`/ppfx/oNS-r3VlTC67VwnnCfx1wAd1jDbbMTSfeXRcovqQe67gIMHc8vr_T66y_0QA1rCquQ?a=V2Vidmlldw`, false},
		// https://github.com/client9/libinjection/issues/155
		{`filter=in(labels.name,"test")`, false},
		// https://github.com/client9/libinjection/issues/151
		{`foo=...ZQ/ONSQg==`, false},
		// https://github.com/client9/libinjection/issues/140
		{`ONLINETOOLS_PHPSESSID=3`, false},
		// https://github.com/client9/libinjection/issues/75
		{`on=x`, false},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%t for %s", tc.malicious, tc.payload), func(t *testing.T) {
			gotXSS, _ := IsXSS(tc.payload)

			if tc.malicious && !gotXSS {
				t.Error("false negative")
			} else if !tc.malicious && gotXSS {
				t.Error("false positive")
			}
		})
	}
}

const (
	html5 = "html5"
	xss   = "xss"
)

var xssCount = 0

func h5TypeToString(h5Type int) string {
	switch h5Type {
	case html5TypeDataText:
		return "DATA_TEXT"
	case html5TypeTagNameOpen:
		return "TAG_NAME_OPEN"
	case html5TypeTagNameClose:
		return "TAG_NAME_CLOSE"
	case html5TypeTagNameSelfClose:
		return "TAG_NAME_SELFCLOSE"
	case html5TypeTagData:
		return "TAG_DATA"
	case html5TypeTagClose:
		return "TAG_CLOSE"
	case html5TypeAttrName:
		return "ATTR_NAME"
	case html5TypeAttrValue:
		return "ATTR_VALUE"
	case html5TypeTagComment:
		return "TAG_COMMENT"
	case html5TypeDocType:
		return "DOCTYPE"
	default:
		return ""
	}
}

func printHTML5Token(h *h5State) string {
	return fmt.Sprintf("%s,%d,%s",
		h5TypeToString(h.tokenType),
		h.tokenLen,
		h.tokenStart[:h.tokenLen])
}

func runXSSTest(filename, flag string) {
	var (
		actual = ""
		data   = readTestData(filename)
	)

	switch flag {
	case xss:

	case html5:
		h5 := new(h5State)
		h5.init(data["--INPUT--"], html5FlagsDataState)

		for h5.next() {
			actual += printHTML5Token(h5) + "\n"
		}
	}

	actual = strings.TrimSpace(actual)
	if actual != data["--EXPECTED--"] {
		xssCount++
		fmt.Println("FILE: (" + filename + ")")
		fmt.Println("INPUT: (" + data["--INPUT--"] + ")")
		fmt.Println("EXPECTED: (" + data["--EXPECTED--"] + ")")
		fmt.Println("GOT: (" + actual + ")")
	}
}

func TestXSSDriver(t *testing.T) {
	baseDir := "./tests/"
	dir, err := ioutil.ReadDir(baseDir)
	if err != nil {
		t.Fatal(err)
	}

	for _, fi := range dir {
		if strings.Contains(fi.Name(), "-html5-") {
			runXSSTest(baseDir+fi.Name(), html5)
		}
	}

	t.Log("False testing count: ", xssCount)
}
