package libinjection

import "testing"

func BenchmarkSQLi(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IsSQLi("' OR 1=1")
	}
}

func BenchmarkXSS(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IsXSS("<script>")
	}
}
